package main
import (
    "bufio"
    "log"
    "fmt"
    "os"
    "strings"
    "time"
)

// Read a whole file into the memory and store it as array of lines
func readLines(path string) ([]string, error) {
    file, err := os.Open(path)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    var lines []string
    currentTime := time.Now()
    lines = append(lines, fmt.Sprintf("#Template edited: %s", currentTime.Format("2006-01-02 3:4:5")))
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines, scanner.Err()
}

func writeLines(lines []string, path string) error {
    if _, err := os.Stat(path); err == nil {
        // if file exists - let's remove it first
        os.Remove(path)
    }
    file, err := os.Create(path)
    if err != nil {
        return err
    }
    defer file.Close()

    w := bufio.NewWriter(file)
    for _, line := range lines {
        fmt.Fprintln(w, line)
    }
    return w.Flush()
}

func main() {
    // read input from parameters
    pathToTemplateFile := os.Args[1:][0]
    // basing on my knowledge of C# named parameters - I would not deliberately overcomplicate here, but ideally path to template file shall be named and only parameters shall be checked
    if len(strings.TrimSpace(pathToTemplateFile)) == 0 { 
        log.Fatal("No parameters provided.")
    }

    lines, err := readLines(pathToTemplateFile)
    if err != nil {
        log.Fatalf("readLines: %s", err)
    }
    // let's print some files in debug mode
    for i, line := range lines {
        fmt.Println(i, line)
    }

    if err := writeLines(lines, "Dockerfile"); err != nil {
        log.Fatalf("writeLines: %s", err)
    }
}
